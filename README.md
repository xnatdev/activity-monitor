# Activity Monitor

Simple [Electron](http://electron.atom.io) application that shows a
doughnut chart of the CPU system, user, and idle activity time.

![Screenshot](https://cloud.githubusercontent.com/assets/671378/20894933/3882a328-bacc-11e6-865b-4bc1c5ac7ec7.png)

This is a copy of the activity monitor Electron sample application. It's a simple placeholder app to build
while developing the Electron build process for OS X and other platforms.

## Getting started

- Install [Node LTS](https://nodejs.org)
- Clone this repository
- `cd activity-monitor`
- `yarn install` to install the application's dependencies
- `yarn start` to start the application
- `yarn dist-mac` to build the application

